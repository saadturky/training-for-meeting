*** Settings ***
Library    SeleniumLibrary
Suite Teardown  Close Browser


*** Test Cases ***
Test Chrome
    Open Browser        https://saadturky.gitlab.io/training-for-meeting/     Chrome      options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors"); add_argument("--disable-dev-shm-usage"); add_argument("--no-sandbox"); add_argument("--headless")
    Set Selenium Speed  10
    Title Should Be     React App
	#Click Element            REGISTER
    [Teardown]          Close All Browsers
