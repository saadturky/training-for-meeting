import React from 'react';

const person = (avariable) => {
    return(
      <div>
          <p>I am {avariable.name} and I am {avariable.age} years old</p>
          <p>{avariable.children}</p>
      </div> 
    ) 
};

export default person;